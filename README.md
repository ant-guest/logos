
The files here are either copies of open logos already done or the the new ones I made from combining those.


Tux and Debian Logos combined:


  The originals:
  
    Tux logo from: https://upload.wikimedia.org/wikipedia/commons/archive/a/af/20070323032438%21Tux.png
  
    - Debian logos from: https://www.debian.org/logos/
      - openlogo-nd.xcf
      - openlogo.xcf
  
  
  The new ones:
  
    - tux-plus-openlogo-nd.png
    - tux-plus-openlogo-nd.xcf
    - tux-plus-openlogo.png
    - tux-plus-openlogo.xcf
  

